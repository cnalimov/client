import { FlagListComponent } from './components/flag-list/flag-list.component';
import { NotFoundComponent } from './components/notfound/notfound.component';
import { PathMatch } from '../jspm_packages/npm/angular2@2.0.0-beta.17/src/router/rules/rules';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    { path: 'flag-list', component: FlagListComponent },
    { path: '', component: FlagListComponent, pathMatch: 'full' },
    { path: '**', component: NotFoundComponent }
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true });