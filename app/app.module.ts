import { RouterModule } from '../jspm_packages/npm/@angular/router@3.0.0';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { CoreModule } from './modules/core.module';
import { ViewsModule } from './modules/views.module';

import { AppComponent } from './app.component';

import { routing } from './app.router';


@NgModule({
  imports: [BrowserModule, HttpModule,  CoreModule, ViewsModule, RouterModule, routing],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
})

export class AppModule { }