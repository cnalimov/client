import { ReactiveErrors } from '../../jspm_packages/npm/@angular/forms@2.0.0/src/directives/reactive_errors';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './../components/menu/menu.component';

@NgModule({
    imports: [FormsModule, ReactiveFormsModule, RouterModule],
    declarations: [MenuComponent],
    exports: [MenuComponent],
    providers: []
})
export class CoreModule { }