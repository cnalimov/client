import { FlagsService } from '../services/flags/flasg.service';
import { CurrencyService } from '../services/currency/currency.service';
import { StickDirective } from '../directives/stick.directive';
import { CommonModule } from '../../jspm_packages/npm/@angular/common@2.1.0';

import { FormsModule } from '../../jspm_packages/npm/@angular/forms@2.0.0';
import { FlagListComponent } from '../components/flag-list/flag-list.component';
import { NotFoundComponent } from '../components/notfound/notfound.component';
import { CoreModule } from './core.module';

import { NgModule } from '@angular/core';


@NgModule({
    imports: [CoreModule, FormsModule, CommonModule],
    declarations: [FlagListComponent, NotFoundComponent, StickDirective],
    exports: [StickDirective],
    providers: [CurrencyService,FlagsService]
})
export class ViewsModule { }