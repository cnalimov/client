import { FlagsService } from '../../services/flags/flasg.service';
import { CurrencyService } from '../../services/currency/currency.service';
import { Inject, Injectable } from '../../../jspm_packages/npm/@angular/core@2.1.0';
import { Provider } from '../../../jspm_packages/npm/angular2@2.0.0-beta.17/ts/web_worker/worker';
import { Component } from '@angular/core';
import { Headers, Http, RequestOptionsArgs, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import Lazy from 'lazy.js';

@Component({
    templateUrl: './app/components/flag-list/flag-list.component.html'
})
export class FlagListComponent {
    searchText: string;
    exchangeData: {};
    flagItems: {}[];
    flags: any;
    allData: any;

    service: CurrencyService;

    baseUrl = 'https://localhost:8011';

    constructor(private _exchangeRateService: CurrencyService, private _flagsService: FlagsService) {

        this.searchText = '';

        // var exchangeRatesObservable = _exchangeRateService.getExchangeRates().do((items) => {

        // });

        // var flasgObservable = _flagsService.getFlags().do((items) => {
        //     this.allData = items;
        // });

        // Observable.forkJoin(exchangeRatesObservable, flasgObservable).subscribe(() => {
        //     alert('done');
        // })

        this.onSearchTextChanged();
    };

    private onSearchTextChanged() {
        // this.flagItems = Lazy(this.allData.fx).filter((item) => {

        //     return (item.nameI18N && item.nameI18N.includes(this.searchText)) || (item.currency) && item.currency.includes(this.searchText)
        // }).map((i) => {
        //     return {
        //         currency: i.currency,
        //         name: i.nameI18N,
        //         rate: i.exchangeRate ? i.exchangeRate.buy : 0
        //     };
        // }).toArray();
    }

}
