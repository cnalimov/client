import { GetExchangeRatesResponse } from './currency.models';
import { Observer } from '../../../jspm_packages/npm/rxjs@5.0.0-beta.12/src/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers, Http, RequestOptionsArgs, Response, URLSearchParams } from '@angular/http';

@Injectable()
export class CurrencyService {
    constructor(private _http: Http) { }

    baseUrl = 'https://localhost:8011';
    
    public getExchangeRates(): Observable<GetExchangeRatesResponse> {
        return this._http.get(this.baseUrl + '/fat2/rest/info/fx').map((res) => {
            let body = res.json();
            return body.data;
        });
    }  
} 