import { FlagsRequest } from './flasg.models';

import { Observer } from '../../../jspm_packages/npm/rxjs@5.0.0-beta.12/src/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers, Http, RequestOptionsArgs, Response, URLSearchParams } from '@angular/http';

@Injectable()
export class FlagsService {
    constructor(private _http: Http) { }
    
    public getFlags(): Observable<FlagsRequest> {
        return this._http.get('/data/countries.json').map((res) => {
            let body = res.json();
            return body.data;
        });
    }  
} 