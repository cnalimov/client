var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./app/styles/all.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/styles'));
});

gulp.task('watch', function () {
    gulp.watch('./app/**/*.scss', ['sass']);
});

gulp.task('default', ['sass']);